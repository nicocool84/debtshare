"""
Login related controllers.
"""

from flask import session, request, url_for, render_template, redirect, \
    flash

from debtshare import app, bcrypt
from debtshare.models import User


@app.route('/login', methods=['GET', 'POST'])
def login():
    """
    Logs a user in. User must be in the User table.
    """
    error = None
    if request.method == 'POST':
        user = User.query.filter_by(username=request.form['login']).first()
        if user \
            and bcrypt.check_password_hash(user.password,
                                           request.form['password']):
            flash("Bien loggé")
            session['logged_in'] = True
            session['userid'] = user.id
            session['username'] = user.username
            return redirect(url_for('home'))
        else:
            error = 'Invalid username'
    return render_template('login.html', error=error)


@app.route('/logout')
def logout():
    """
    Clear the session.
    """
    session.pop('logged_in', None)
    session.clear()
    flash('You were logged out')
    return redirect(url_for('login'))
