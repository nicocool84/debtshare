SQLALCHEMY_DATABASE_URI = "sqlite:////tmp/debtshare.sqlite3"
SECRET_KEY = "very unsafe key"

NAME = "Debtshare testing machine"

# convenient way to create users but delete this entry after running
# FLASK_APP=flask init for the first time.
USERS = (("user1", "pass1"),
         ("user2", "pass2"),
         ("user3", "pass3"))

