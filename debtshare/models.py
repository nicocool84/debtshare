"""
Models and DB access
"""

from flask_sqlalchemy import SQLAlchemy
from debtshare import app, bcrypt

from datetime import datetime

db = SQLAlchemy(app)

debtors = db.Table('debtors',
                   db.Column('expense_id',
                             db.Integer,
                             db.ForeignKey('expense.id')),
                   db.Column('user_id',
                             db.Integer,
                             db.ForeignKey('user.id')))


class User(db.Model):
    """
    User class.
    """
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80), unique=True, nullable=False)
    password = db.Column(db.String(80), nullable=False)

    def __repr__(self):
        return '<User {} [{}]>'.format(self.username, self.id)

    def get_debt_with(self, user):
        amount = 0
        # Expenses
        given = Expense.query.filter_by(payed_by=self).all()
        for expense in given:
            if user in expense.debtors:
                amount -= expense.amount / len(expense.debtors)
        received = Expense.query.filter_by(payed_by=user).all()
        for expense in received:
            if self.id != expense.payed_by_id and self in expense.debtors:
                amount += expense.amount / len(expense.debtors)
        # Transactions
        given = Transaction.query.filter_by(giver=self, receiver=user).all()
        for transaction in given:
            amount -= transaction.amount
        received = Transaction.query.filter_by(giver=user, receiver=self).all()
        for transaction in received:
            amount += transaction.amount

        return round(amount, 2)


class Expense(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), nullable=False)
    amount = db.Column(db.Float, nullable=False)
    date = db.Column(db.Date, nullable=False)
    payed_by_id = db.Column(db.Integer,
                            db.ForeignKey('user.id'),
                            nullable=False)
    payed_by = db.relationship('User',
                               backref=db.backref('expenses', lazy=True))
    debtors = db.relationship('User',
                              secondary=debtors)

    def __repr__(self):
        return '<Expense {} ({}€) ' \
               'payed by {} for {}>'.format(self.name,
                                            self.amount,
                                            self.payed_by.username,
                                            self.debtors)


class Transaction(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.Date, nullable=False, default=datetime.now())
    giver_id = db.Column(db.Integer,
                         db.ForeignKey('user.id'),
                         nullable=False)
    giver = db.relationship('User', foreign_keys=[giver_id])
    receiver_id = db.Column(db.Integer,
                            db.ForeignKey('user.id'),
                            nullable=False)
    receiver = db.relationship('User', foreign_keys=[receiver_id])
    amount = db.Column(db.Float, nullable=False)

    def __repr__(self):
        return '<{}€ from {} to {}>'.format(self.amount,
                                            self.giver,
                                            self.receiver)


@app.cli.command("initdb")
def init_db():
    confirmation = input("This will destroy the database, are you sure? [y/N]")
    if confirmation[0] not in ("y", "Y"):
        return
    db.drop_all()
    db.create_all()
    for username, password in app.config['USERS']:
        db.session.add(User(username=username,
                            password=bcrypt.generate_password_hash(password)))
    db.session.commit()
    print("All good.")
