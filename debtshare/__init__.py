"""
Base flask app file.
"""

import os

from flask import Flask
from flask_scss import Scss
from flask_bcrypt import Bcrypt

app = Flask(__name__)
app.config.from_object(__name__)

Scss(app,
     static_dir=os.path.join(app.root_path, 'static'),
     asset_dir=os.path.join(app.root_path, 'assets'))

app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config.from_envvar('DEBTSHARE_CONFIG')

bcrypt = Bcrypt(app)


import debtshare.models
import debtshare.login
import debtshare.debts
