"""
Debt and expenses-related controllers.
"""

from flask import session, request, url_for, \
    render_template, redirect, flash, abort

from datetime import datetime
from dateutil.relativedelta import relativedelta

from debtshare.models import Expense, User, Transaction, db
from debtshare import app

now = datetime.now()


@app.route('/')
def home():
    return redirect(url_for('show_debts', year=now.year, month=now.month))


def date_fmt(year=now.year, month=now.month, day=now.day):
    return "{:04d}{:02d}{:02d}".format(int(year),
                                       int(month),
                                       int(day))


@app.route('/list/<int:year>/<int:month>')
def show_debts(year=now.year, month=now.month):
    if not session.get('logged_in'):
        return redirect(url_for('login'))

    current = datetime(day=1, month=month, year=year)

    next_month = current + relativedelta(months=1)
    prev_month = current - relativedelta(months=1)

    users = User.query.all()

    for user in users:
        if user.id == session.get('userid'):
            me = user
            break

    expenses = Expense.query.filter(
        Expense.date.between(current,
                             next_month - relativedelta(days=1)),
        (Expense.debtors.contains(me) | (Expense.payed_by == me))
    ).order_by(Expense.date)

    monthly_price = 0

    for expense in expenses:
        if me in expense.debtors:
            monthly_price += expense.amount / len(expense.debtors)

    return render_template('list.html',
                           users=users,
                           debts=expenses,
                           month=month,
                           year=year,
                           day=now.day,
                           current=current,
                           prev_month=prev_month,
                           next_month=next_month,
                           me=me,
                           monthly_price=monthly_price)


@app.route('/receive', methods=['POST'])
def receive():
    if not session.get('logged_in'):
        abort(401)
    transaction = Transaction(amount=request.form['amount'],
                              receiver_id=session.get('userid'),
                              giver_id=request.form['giver'],
                              date=now)

    db.session.add(transaction)
    db.session.commit()

    flash('Transaction enregistrée.')

    return redirect(url_for('history',
                            year=request.form['year'],
                            month=request.form['month']))


@app.route('/history')
def history():
    if not session.get('logged_in'):
        abort(401)

    given = Transaction.query.filter_by(
        giver_id=session.get('userid')).all()
    received = Transaction.query.filter_by(
        receiver_id=session.get('userid')).all()

    users = User.query.all()
    for user in users:
        if user.id == session.get('userid'):
            me = user
            break

    return render_template('history.html',
                           given=given,
                           received=received,
                           me=me,
                           users=users)


@app.route('/add', methods=['POST'])
def add():
    if not session.get('logged_in'):
        abort(401)

    debtors = [User.query.filter_by(id=x).first()
               for x in request.form.getlist('debtors')]
    expense = Expense(date=datetime(year=int(request.form['year']),
                                    month=int(request.form['month']),
                                    day=int(request.form['day'])),
                      name=request.form['name'],
                      debtors=debtors,
                      amount=request.form['amount'],
                      payed_by_id=session['userid'])

    db.session.add(expense)
    db.session.commit()

    flash('Dette bien ajoutée')
    return redirect(url_for('show_debts',
                            year=request.form['year'],
                            month=request.form['month']))


@app.route('/delete/', methods=['POST'])
def delete():
    if not session.get('logged_in'):
        abort(401)

    expense = Expense.query.filter_by(id=request.form['id']).first()
    db.session.delete(expense)
    db.session.commit()

    flash('Dette supprimée.')
    return redirect(url_for('show_debts',
                            year=now.year,
                            month=now.month))
