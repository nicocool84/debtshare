from setuptools import setup

setup(
    name='debtshare',
    packages=['debtshare'],
    include_package_data=True,
    install_requires=[
        'flask', 'flask-sqlalchemy', 'flask-scss', 'python-dateutil',
        'flask-bcrypt'
    ],
    setup_requires=[
        'pytest-runner',
    ],
    tests_require=[
        'pytest',
    ],
)
