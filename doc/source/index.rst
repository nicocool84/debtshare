.. Debtshare documentation master file, created by
   sphinx-quickstart on Wed Mar 29 21:31:48 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Debtshare's documentation!
=====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

.. disabled because useless
 .. automodule:: debtshare
 .. automodule:: debtshare.models
 .. automodule:: debtshare.login
 .. automodule:: debtshare.debts


Installation
============

Install through pip.

.. code::

   $ pip install debtshare
   $ export FLASK_APP=debtshare
   $ export DEBTSHARE_CONFIG=/path/to/debtshare_config_file.py
   $ flask initdb

Test with:

.. code::

   $ flask run

Configure


Contributing
============

.. code::

   git clone