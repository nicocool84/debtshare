import os
import debtshare
import unittest
import tempfile

from debtshare.models import Expense, db, Transaction, User, Expense
from datetime import datetime


class DebtshareTestCase(unittest.TestCase):

    def setUp(self):
        self.db_fd, debtshare.app.config['DATABASE'] = tempfile.mkstemp()
        debtshare.app.config['TESTING'] = True
        self.app = debtshare.app.test_client()

    def tearDown(self):
        os.close(self.db_fd)
        os.unlink(debtshare.app.config['DATABASE'])

    def login(self, username, password):
        return self.app.post('/login', data=dict(
            login=username,
            password=password
        ), follow_redirects=True)

    def logout(self):
        return self.app.get('/logout', follow_redirects=True)

    def test_login(self):
        rv = self.login('user1', 'pass1')
        assert b'Bien' in rv.data

    def test_model_transaction(self):
        with debtshare.app.test_request_context('/'):
            db.session.flush()
            users = User.query.all()
            debt_before = users[0].get_debt_with(users[1])
            amount = 155
            trans = Transaction(giver=users[0], receiver=users[1],
                                amount=amount)
            db.session.add(trans)
            assert users[0].get_debt_with(users[1]) + amount == debt_before

    def test_model_expense(self):
        with debtshare.app.test_request_context('/'):
            db.session.flush()
            users = User.query.all()
            debt_before = users[0].get_debt_with(users[1])
            amount = 155
            expense = Expense(payed_by=users[0], debtors=[users[0], users[1]],
                              amount=amount, name="Tata", date=datetime.now())
            db.session.add(expense)
            assert users[0].get_debt_with(users[1]) + amount / 2 == debt_before


if __name__ == '__main__':
    unittest.main()
